<?php namespace App\Controllers;

use \App\Libraries\Oauth;
use \OAuth2\Request;
use \CodeIgniter\API\ResponseTrait;
use App\Models\UtilisateurModel;

class User extends BaseController
{
	use ResponseTrait;

	public function login()
	{
		$oauth = new Oauth();
		$request = new Request();
		$respond = $oauth->server->handleTokenRequest($request->createFromGlobals());
		$code = $respond->getStatusCode();
		$body = $respond->getResponseBody();
		return $this->respond(json_decode($body), $code);
	}


	public function register(){
		helper('form');
		$model = new UtilisateurModel();

		if($this->request->getMethod() != 'post'){
			return $this->fail('Requête POST seulement acceptée');
		}

		if(!$this->validate($model->rules)){
			return $this->fail($this->validator->getErrors());
		}else{
			$data = [
				'nom' => $this->request->getVar('nom'),
				'prenom' => $this->request->getVar('prenom'),
				'utilisateur' => $this->request->getVar('utilisateur'),
				'password' => $this->request->getVar('password'),
			];
			$data['password_not_hashed'] = $data['password'];
			$id_utilisateur = $model->insert($data);
			$data = $model->find($id_utilisateur);
			//unset($data['password']);
			return $this->respondCreated($data);
		}
	}
	//--------------------------------------------------------------------

}
