<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Actualites extends ResourceController
{
    protected $modelName = 'App\Models\ActualitesModel';
    protected $format = 'json';

    public function index()
    {
        $actualites = $this->model->findAll();
        return $this->respond($actualites);
    }

    public function create()
    {
        if (!$this->validate($this->model->rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            $data = [
                'titre' => $this->request->getVar('titre'),
                'date' => $this->request->getVar('date'),
                'contenu' => $this->request->getVar('contenu'),
                'image' => $this->request->getVar('image')
            ];
            $id = $this->model->insert($data);
            $data['id'] = $id;
            return $this->respondCreated($data);
        }
    }

    public function show($id = null)
    {
        $data = $this->model->find($id);
        return $this->respond($data);
    }

    public function update($id = null)
    {
        if (!$this->validate($this->model->rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            $input = $this->request->getRawInput();
            $data = [
                'id' => $id,
                'titre' => $input['titre'],
                'date' => $input['date'],
                'contenu' => $input['contenu'],
                'image' => $input['image'],
            ];
            $this->model->save($data);
            return $this->respondUpdated($data);
        }
    }

    public function delete($id = null)
    {
        $data = $this->model->find($id);
        if ($data) {
            $this->model->delete($id);
            return $this->respondDeleted($data);
        } else {
            return $this->failNotFound('Actualité non trouvé');
        }
    }
}
