<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Region extends ResourceController
{
    protected $modelName = 'App\Models\RegionModel';
    protected $format = 'json';

    public function index()
    {
        $regions = $this->model->findAll();
        return $this->respond($regions);
    }
}