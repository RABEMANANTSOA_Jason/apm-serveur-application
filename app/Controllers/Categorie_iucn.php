<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\Statut_Categorie_iucnModel;

class Categorie_iucn extends ResourceController
{
    protected $modelName = 'App\Models\Statut_Categorie_iucnModel';
    protected $format = 'json';

    public function index()
    {
        $categories = $this->model->findAll(7);
        return $this->respond($categories);
    }
}