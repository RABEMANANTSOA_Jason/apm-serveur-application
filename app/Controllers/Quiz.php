<?php namespace App\Controllers;

use App\Models\custom\QuizCustomModel;
use CodeIgniter\RESTful\ResourceController;
use App\Models\QuizDetailModel;

class Quiz extends ResourceController
{
    protected $modelName = 'App\Models\QuizModel';
    protected $format = 'json';
    
    public function index()
    {
        return $this->respond($this->model->findAll(3));
    }

    public function getDetail()
    {
        $db = db_connect();
        $custom = new QuizCustomModel($db);
        return $this->respond($custom->getDetailQuiz($this->request->getVar('id')));
    }
}
