<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\custom\PopulationCustomModel;

class Population extends ResourceController
{
    protected $format = 'json';
    
    public function recherche_population_ap($id = null)
    {
        $db = db_connect();
        $ap = new PopulationCustomModel($db);
        return $this->respond($ap->getPopulationAP($id));
    }
}
