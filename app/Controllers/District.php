<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class District extends ResourceController
{
    protected $modelName = 'App\Models\DistrictModel';
    protected $format = 'json';

    public function index()
    {
        $districts = $this->model->findAll();
        return $this->respond($districts);
    }
}