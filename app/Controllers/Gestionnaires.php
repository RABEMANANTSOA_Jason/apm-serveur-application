<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Gestionnaires extends ResourceController
{
    protected $modelName = 'App\Models\GestionnaireModel';
    protected $format = 'json';
    
    public function index()
    {
        return $this->respond($this->model->findAll());
    }
}
