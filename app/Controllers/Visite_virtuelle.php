<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\custom\VisiteVirtuelleCustomModel;

class Visite_virtuelle extends ResourceController
{
    protected $modelName = 'App\Models\VisiteVirtuelleModel';
    protected $format = 'json';

    public function index()
    {   
        return $this->respond($this->model->findAll());
    }

    public function show($id = null)
    {
        return $this->respond($this->model->find($id));
    }

    public function recherche_visite_ap()
    {
        if($this->request->getVar('id') != null){
            $db = db_connect();
            $custom = new VisiteVirtuelleCustomModel($db);
            return $this->respond($custom->getVisiteAP($this->request->getVar('id')));
        }
    }
}