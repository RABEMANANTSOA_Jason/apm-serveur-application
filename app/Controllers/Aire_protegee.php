<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\custom\AireProtegeCustomModel;

class Aire_protegee extends ResourceController
{
    protected $modelName = 'App\Models\AireProtegeModel';
    protected $format = 'json';

    public function index()
    {
        $db = db_connect();
        $page = $this->request->getVar('page');
        $limit = $this->request->getVar('limit');
        $ap = new AireProtegeCustomModel($db);
        return $this->respond($ap->getAPindex($page, $limit));
    }

    public function rechercheglobal()
    {
        $db = db_connect();
        $ap = new AireProtegeCustomModel($db);
        if ($this->request->getVar('mot') != null) {
            return $this->respond($ap->rechercheglobal($this->request->getVar('mot')));
        }
        if ($this->request->getVar('region') != null) {
            return $this->respond($ap->rechercheRegion($this->request->getVar('region')));
        }
        if ($this->request->getVar('district') != null) {
            return $this->respond($ap->rechercheglobal($this->request->getVar('district')));
        }
        if ($this->request->getVar('commune') != null) {
            return $this->respond($ap->rechercheglobal($this->request->getVar('commune')));
        }
        return $this->respond([]);
    }

    public function show($id = null)
    {
        $db = db_connect();
        $ap = new AireProtegeCustomModel($db);
        return $this->respond($ap->getAPDetail($id));
    }

    public function get_min_superficie()
    {
        $db = db_connect();
        $ap = new AireProtegeCustomModel($db);
        return $this->respond($ap->getMinSuperficie());
    }

    public function get_max_superficie()
    {
        $db = db_connect();
        $ap = new AireProtegeCustomModel($db);
        return $this->respond($ap->getMaxSuperficie());
    }

    public function tri()
    {
        $db = db_connect();
        $ap = new AireProtegeCustomModel($db);
        return $this->respond($ap->tri(
            $this->request->getVar('min'),
            $this->request->getVar('max'),
            $this->request->getVar('categorie'),
            $this->request->getVar('gestionnaire'),
        ));
    }
}
