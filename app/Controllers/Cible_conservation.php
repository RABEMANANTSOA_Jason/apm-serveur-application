<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\custom\CibleConservationCustomModel;

class Cible_conservation extends ResourceController
{
    protected $format = 'json';
    
    public function recherche_cibles_ap($id = null)
    {
        $db = db_connect();
        $ap = new CibleConservationCustomModel($db);
        return $this->respond($ap->getCibleConservationAP($id));
    }
}
