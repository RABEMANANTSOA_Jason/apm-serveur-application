<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\custom\GeolocalisationCustomModel;

class Geolocalisation extends ResourceController
{
    protected $format = 'json';

    public function index()
    {   
        $db = db_connect();
        $model = new GeolocalisationCustomModel($db);
        return $this->respond($model->index());
    }

    public function recherche($id = null)
    {   
        $db = db_connect();
        $model = new GeolocalisationCustomModel($db);
        return $this->respond($model->getAPGeolocalisation($id));
    }

}