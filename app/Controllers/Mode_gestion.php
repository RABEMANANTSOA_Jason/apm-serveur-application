<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Mode_gestion extends ResourceController
{
    protected $modelName = 'App\Models\Mode_GestionModel';
    protected $format = 'json';
    
    public function index()
    {
        return $this->respond($this->model->findAll(4));
    }
}
