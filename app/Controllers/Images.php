<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\custom\ImageCustomModel;

class Images extends ResourceController
{
    protected $format = 'json';

    public function recherche_gallery_ap()
    {
        if($this->request->getVar('id') != null){
            $db = db_connect();
            $custom = new ImageCustomModel($db);
            return $this->respond($custom->getImagesAP($this->request->getVar('id')));
        }
    }
}