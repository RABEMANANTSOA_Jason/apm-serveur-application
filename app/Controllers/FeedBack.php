<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class FeedBack extends ResourceController
{
    protected $modelName = 'App\Models\FeedBackModel';
    protected $format = 'json';

    public function index()
    {
        $feedback = $this->model->findAll();
        return $this->respond($feedback);
    }

    public function create()
    {
        if (!$this->validate($this->model->rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            $data = [
                'nom' => $this->request->getVar('nom'),
                'prenoms' => $this->request->getVar('prenoms'),
                'email' => $this->request->getVar('mail'),
                'sujet' => $this->request->getVar('sujet'),
                'message' => $this->request->getVar('message'),
            ];
            $id = $this->model->insert($data);
            $data['id'] = $id;
            return $this->respondCreated($data);
        }
    }
    //--------------------------------------------------------------------

}
