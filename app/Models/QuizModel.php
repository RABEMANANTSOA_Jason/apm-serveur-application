<?php

namespace App\Models;

use CodeIgniter\Model;

class QuizModel extends Model
{
    protected $table = 'Quiz';
    protected $primaryKey = 'id_quiz';
    protected $allowedFields = ['nom_quiz'];
    protected $rules = [
        'nom_quiz' => [
            'rules' => 'required|is_unique[quiz.nom_quiz]',
            'errors' => [
                'required' => "Veuillez donner un titre au quiz",
                'is_unique' => "Ce quiz existe déjà"
            ]
        ]
    ];

    public function getProvinceAll(){
        return $this->findAll();
    }
}