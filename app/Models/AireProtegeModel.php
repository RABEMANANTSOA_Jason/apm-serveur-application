<?php

namespace App\Models;

use CodeIgniter\Model;

class AireProtegeModel extends Model
{
    protected $table = 'Aires_Protegees';
    protected $primaryKey = 'id_ap';
    protected $allowedFields = [
        'nom_du_site', 'type_ap', 'historique', 'superficie_decret',
        'superficie_nd', 'superficie_zt', 'date_creation', 'arrete_mise_en_protection',
        'date_mise_en_protection', 'arrete_decret_en_cours', 'numero_decret', 'date_signature'
    ];

    protected $rules = [
        'nom_du_site' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un nom pour le site",
            ]
        ],
        'type_ap' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un type pour le site",
            ]
        ],
        'historique' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un historique",
            ]
        ],
        'date_creation_ap' => [
            'rules' => 'required|valid_date',
            'errors' => [
                'required' => "Veuillez donner une date de création",
                'valid_date' => "Veuillez donner une date valide ex: 2010-12-29"
            ]
        ],
        'geojson' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez entrer les données géographiques de géolocalisation",
            ]
        ],
    ];

    public function getNombreAireProtegee(){
        return $this->countAllResults();
    }

    public function getAiresProtegees(){
        $aire_protegee = $this->findAll();
        $statut_categorie_iucn = new Statut_Categorie_iucnModel();
        $gestionnaire = new GestionnaireModel();
        $mode_gestion = new Mode_GestionModel();

        $i = 0;
        foreach ($aire_protegee as $item) {
            $id_statut_categorie_iucn = $aire_protegee[$i]['id_statut_categorie_iucn'];
            $id_gestionnaire = $aire_protegee[$i]['id_gestionnaire'];
            $id_mode_gestion = $aire_protegee[$i]['id_mode_gestion'];
            $aire_protegee[$i]['statut_categorie_iucn'] = $statut_categorie_iucn->find($id_statut_categorie_iucn);
            $aire_protegee[$i]['gestionnaire'] = $gestionnaire->find($id_gestionnaire);
            $aire_protegee[$i]['mode_gestion'] = $mode_gestion->find($id_mode_gestion);
            $i++;
        }
        $i = 0;
        foreach ($aire_protegee as $item) {
            unset($aire_protegee[$i]['id_statut_categorie_iucn']);
            unset($aire_protegee[$i]['id_gestionnaire']);
            unset($aire_protegee[$i]['id_mode_gestion']);
            $i++;
        }
        return $aire_protegee;
    }
}
