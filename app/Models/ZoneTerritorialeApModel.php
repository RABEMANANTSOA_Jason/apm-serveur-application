<?php

namespace App\Models;

use CodeIgniter\Model;

class ZoneTerritorialeApModel extends Model
{
    protected $table = 'Zone_Territoriale_AP';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_aire_protegee', 'id_region', 'id_district', 'id_commune', 'id_fokontany'];
    protected $rules = [
        'district' => [
            'rules' => 'required|is_unique[districts.district]',
            'errors' => [
                'required' => "Veuillez donner un nom de district",
                'is_unique' => "Cette district existe déjà"
            ]
        ],
        'id_region' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez référence une région",
            ]
        ]
    ];
}
