<?php

namespace App\Models;

use CodeIgniter\Model;

class FeedBackModel extends Model
{
    protected $table = 'Feedback';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nom', 'prenoms', 'email', 'sujet', 'message','date_creation','vu'];
    protected $rules = [
        'nom' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un nom s'il vous plaît",
            ]
        ],
        'prenoms' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un prénom s'il vous plaît",
            ]
        ],
        'email' => [
            'rules' => 'valid_email',
            'errors' => [
                'valid_email' => "Veuillez fournir un email valide",
            ]
        ],
        'sujet' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un sujet s'il vous plaît",
            ]
        ],
        'message' => [
            'rules' => 'required|max_length[3000]',
            'errors' => [
                'required' => "Veuillez écrire un message s'il vous plaît",
                'max_length' => "Le nombre maximal de caractère est de 3000"
            ]
        ],
    ];
}
