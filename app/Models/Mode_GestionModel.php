<?php

namespace App\Models;

use CodeIgniter\Model;

class Mode_GestionModel extends Model
{
    protected $table = 'Mode_Gestion';
    protected $primaryKey = 'id';
    protected $allowedFields = ['type', 'description'];
    protected $rules = [
        'nom' => [
            'rules' => 'required|is_unique[mode_gestion.type]',
            'errors' => [
                'required' => "Veuillez donner un nom au type de gestion",
                'is_unique' => "Ce nom existe déjà"
            ]
        ],
    ];

    public function getMode_Gestion_All(){
        return $this->findAll();
    }
}
