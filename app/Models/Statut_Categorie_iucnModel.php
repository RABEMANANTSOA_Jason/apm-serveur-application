<?php

namespace App\Models;

use CodeIgniter\Model;

class Statut_Categorie_iucnModel extends Model
{
    protected $table = 'Statut_Categorie_IUCN';
    protected $primaryKey = 'id';
    protected $allowedFields = ['code', 'statut_fr', 'categorie', 'statut_mg'];
    protected $rules = [
        'code' => [
            'rules' => 'required|is_unique[statut_categorie_iucn.code]',
            'errors' => [
                'required' => "Veuillez donner un titre à l'actualité",
                'is_unique' => "Cette abréviation existe déjà"
            ]
        ],
        'statut_fr' => [
            'rules' => 'required|is_unique[statut_categorie_iucn.statut_fr]',
            'errors' => [
                'required' => "Veuillez donner une date à l'actualité",
                'is_unique' => "Ce statut existe déjà"
            ]
        ],
        'categorie' => [
            'rules' => 'required|is_unique[statut_categorie_iucn.categorie]',
            'errors' => [
                'required' => "Veuillez fournir un contenu à l'actualité",
                'is_unique' => "Cette catégorie existe déjà"
            ]
        ]
    ];

    public function getStatut_Categorie_IUCN_All(){
        return $this->findAll();
    }
}
