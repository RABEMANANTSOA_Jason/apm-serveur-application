<?php

namespace App\Models;

use CodeIgniter\Model;

class GestionnaireModel extends Model
{
    protected $table = 'Gestionnaire';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nom', 'description', 'date_creation'];
    protected $rules = [
        'nom' => [
            'rules' => 'required|is_unique[gestionnaire.nom]',
            'errors' => [
                'required' => "Veuillez donner un nom au gestionnaire",
                'is_unique' => "Ce nom existe déjà"
            ]
        ],
    ];

    public function getGestionnaires_All(){
        return $this->findAll();
    }
}
