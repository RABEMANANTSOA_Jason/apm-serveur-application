<?php

namespace App\Models;

use CodeIgniter\Model;

class GeolocalisationModel extends Model
{
    protected $table = 'Geolocalisation';
    protected $primaryKey = 'id';
    protected $allowedFields = ['geojson', 'id_aire_protegee'];
}
