<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class VisiteVirtuelleCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getDetailVisiste($id){
        $builder = $this->db->table('Visite_Virtuelle');
        $builder->where(['Visite_Virtuelle.id' => $id]);
        $builder->join('Aires_Protegees', 'Visite_Virtuelle.id_aire_protegee = Aires_Protegees.id_ap');
        $result = $builder->get()->getResult();
        return $result;
    }

    public function getVisiteAP($id){
        $builder = $this->db->table('Visite_Virtuelle');
        $builder->where(['Visite_Virtuelle.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        if($result != null){
            return $result[0];
        }
    }
}
