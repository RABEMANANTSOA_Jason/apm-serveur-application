<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class AireProtegeCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db = &$db;
    }

    public function getAPindex($page, $limit)
    {
        $builder = $this->db->table('Aires_Protegees');
        $builder->select(['id_ap', 'nom_du_site', 'ap_image']);
        $result = $builder->get($limit, $page)->getResult();
        return $result;
    }

    public function verificationCible($id){
        $builder = $this->db->table('Cible_Conservation');
        $builder->where(['Cible_Conservation.id_aire_protegee' =>$id]);
        return count($builder->get()->getResult());
    }

    public function verificationPopulation($id){
        $builder = $this->db->table('Population');
        $builder->where(['Population.id_aire_protegee' =>$id]);
        return count($builder->get()->getResult());
    }

    public function verificationImages($id){
        $builder = $this->db->table('Images');
        $builder->where(['Images.id_aire_protegee' =>$id]);
        return count($builder->get()->getResult());
    }

    public function getAPDetail($id){
        $builder = $this->db->table('Aires_Protegees');
        $builder->where(['Aires_Protegees.id_ap' => $id]);
        $builder->join('Statut_Categorie_IUCN', 'Aires_Protegees.id_statut_categorie_iucn = Statut_Categorie_IUCN.id');
        $builder->join('Gestionnaire','Aires_Protegees.id_gestionnaire = Gestionnaire.id');
        $builder->join('Mode_Gestion','Aires_Protegees.id_mode_gestion = Mode_Gestion.id');
        $builder->join('Geolocalisation','Geolocalisation.id_aire_protegee = Aires_Protegees.id_ap');
        if($this->verificationCible($id)>0){
            $builder->join('Cible_Conservation','Cible_Conservation.id_aire_protegee = Aires_Protegees.id_ap');
        }
        if($this->verificationPopulation($id)>0){
            $builder->join('Population','Population.id_aire_protegee = Aires_Protegees.id_ap');
        }
        if($this->verificationImages($id)>0){
            $builder->join('Images','Images.id_aire_protegee = Aires_Protegees.id_ap');
        }
        $result = $builder->get()->getResult();
        if($result != null){return $result[0];}
        return $result;
    }

    public function rechercheglobal($mot)
    {
        $builder = $this->db->table('Aires_Protegees');
        $builder->select(['id_ap', 'nom_du_site', 'ap_image']);
        $builder->orLike('id_ap', $mot);
        $builder->orLike('nom_du_site', $mot);
        $builder->orLike('type_ap', $mot);
        $builder->orLike('historique', $mot);
        $builder->orLike('superficie_decret', $mot);
        $builder->orLike('superficie_nd', $mot);
        $builder->orLike('superficie_zt', $mot);
        $builder->orLike('date_creation_ap', $mot);
        $builder->orLike('arrete_mise_en_protection', $mot);
        $builder->orLike('arrete_decret_en_cours', $mot);
        $builder->orLike('numero_decret', $mot);
        $builder->orLike('date_signature', $mot);
        $builder->orLike('regions', $mot);
        $builder->orLike('districts', $mot);
        $builder->orLike('communes', $mot);
        $builder->orLike('fokontany', $mot);
        $builder->orLike('ap_image', $mot);
        $builder->orLike('menaces', $mot);
        $builder->orLike('potentialites', $mot);
        $builder->orWhere('superficie_decret < ', 5);
        $result = $builder->get()->getResult();
        return $result;
    }

    public function rechercheRegion($region)
    {
        $builder = $this->db->table('Aires_Protegees');
        $builder->select(['id_ap', 'nom_du_site', 'ap_image']);
        $builder->orLike('regions', $region);
        $result = $builder->get()->getResult();
        return $result;
    }

    public function getMinSuperficie()
    {
        $builder = $this->db->table('Aires_Protegees');
        $builder->selectMin('superficie_decret');
        $result = $builder->get()->getResult();
        return $result[0]->superficie_decret;
    }

    public function getMaxSuperficie()
    {
        $builder = $this->db->table('Aires_Protegees');
        $builder->selectMax('superficie_decret');
        $result = $builder->get()->getResult();
        return $result[0]->superficie_decret;
    }

    public function tri($min, $max, $categorie, $gestionnaire)
    {
        $builder = $this->db->table('Aires_Protegees');
        if ($min != null) {
            $builder->where('superficie_decret >= ', $min);
        }
        if ($max != null) {
            $builder->where('superficie_decret <= ', $max);
        }
        if ($categorie >= 0) {
            $builder->where('id_statut_categorie_iucn', $categorie);
        }
        if ($gestionnaire >= 0) {
            $builder->where('id_gestionnaire', $gestionnaire);
        }
        $result = $builder->get()->getResult();
        return $result;
    }
}
