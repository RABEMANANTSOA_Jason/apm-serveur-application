<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class QuizCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getDetailQuiz($id){
        $builder = $this->db->table('Quiz_detail');
        $builder->where(['id_quiz' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }
}
