<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class CibleConservationCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getCibleConservationAP($id){
        $builder = $this->db->table('Cible_Conservation');
        $builder->where(['Cible_Conservation.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }
}
