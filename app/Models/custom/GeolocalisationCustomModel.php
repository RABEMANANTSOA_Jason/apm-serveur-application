<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class GeolocalisationCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function prepareGEOJSON(array $data){
        $geojson = [];
        $geojson['type'] = 'FeatureCollection';
        $geojson['features'] = [];
        for($i = 0; $i < count($data); $i++){
            array_push($geojson['features'],json_decode($data[$i]->geojson)); 
        }
        return $geojson;
    }

    public function index(){
        $builder = $this->db->table('Geolocalisation');
        $builder->select(['geojson']);
        $result = $builder->get()->getResult();
        return $this->prepareGEOJSON($result);
    }

    public function getAPGeolocalisation($id){
        $builder = $this->db->table('Geolocalisation');
        $builder->where(['Geolocalisation.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        if($result!= null){
            return $result[0];
        }
    }
}
