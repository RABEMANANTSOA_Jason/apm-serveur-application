<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class ImageCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getImagesAP($id){
        $builder = $this->db->table('Images');
        $builder->where(['Images.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }
}
