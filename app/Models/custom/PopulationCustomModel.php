<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class PopulationCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getPopulationAP($id){
        $builder = $this->db->table('Population');
        $builder->where(['Population.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }
}
