<?php

namespace App\Models;

use CodeIgniter\Model;

class CommuneModel extends Model
{
    protected $table = 'Communes';
    protected $primaryKey = 'id';
    protected $allowedFields = ['commune', 'id_district'];
    protected $rules = [
        'commune' => [
            'rules' => 'required|is_unique[communes.commune]',
            'errors' => [
                'required' => "Veuillez donner un nom de commune",
                'is_unique' => "Cette commune existe déjà"
            ]
        ],
        'id_district' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez référencer une district"
            ]
        ]
    ];

    public function getCommuneAll(){
        return $this->findAll();
    }
}
